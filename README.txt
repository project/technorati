
Copyright 2006 http://2bits.com

Description:
------------

This module enables you to use Technorati tags in your content.

There are different modes for the Technorati tags to work, and they
can be different for each content type.

* None: means do not do any Technorati tags for this content type.
* Manual entry: means that the tags have to be entered manually for each node.
* Drupal categories: means that the category terms the node belong to will be
  used as Technorati tags.
* Both: means a combination of manual entries and categories.

For example, you can set blogs to be both (i.e. Drupal categories and manual
entries), but disable it for page, and use only categories for story type.

The module will also notify Technorati.com that new content has been published
on your site.

Installation:
------------

1) Copy the technorati directory and all its contents to your modules directory.

2) Enable the technorati module by visiting administer -> modules

3) Make sure that the ping module is enabled and cron is running too.

4) Select a mode of deriving the tags for each content type under administer
   -> settings -> technorati

Bugs/Features/Patches:
----------------------
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.

Author
------
Khalid Baheyeldin (http://baheyeldin.com/khalid and http://2bits.com)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.
